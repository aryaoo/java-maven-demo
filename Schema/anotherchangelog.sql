-- liquibase formatted sql
-- changeset guox.goodrain:1
create table another_person (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_person;
-- changeset guox.goodrain:2
create table another_company (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_company;
-- changeset other.goodrain:3
alter table another_person add column country varchar(2);
create table another_staff (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_staff;
-- rollback alter table another_person drop column country;
-- changeset other.goodrain:4
create table another_man (
id int primary key,
name varchar(50) not null,
address1 varchar(50),
address2 varchar(50),
city varchar(30)
);
-- rollback drop table another_man;